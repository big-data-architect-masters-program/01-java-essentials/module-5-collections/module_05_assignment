import java.util.ArrayList;

public class AssignmentArrayList {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        // add element to arraylist
        for (int i=1;i<=10;i++){
            arrayList.add(i);
        }
        System.out.println(arrayList);
        // display element from arraylist
        for (int o: arrayList){
            System.out.println(o);
        }
    }
}
