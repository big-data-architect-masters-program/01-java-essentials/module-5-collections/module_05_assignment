import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class AssignmentHashMap {
    public static void main(String[] args) {
        // create hashmap to store employee
        HashMap employees = new HashMap();
        // add employee
        employees.put(1, "Jonh");
        employees.put(2, "NaNa");
        employees.put(3, "Dara");
        employees.put(4, "Tony");
        employees.put(5, "Stark");
        // display employees
        Iterator iterator = employees.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry entry = (Map.Entry) iterator.next();
            System.out.println("EmployeeID:"+entry.getKey()+", EmployeeName:"+entry.getValue());
        }
    }
}
