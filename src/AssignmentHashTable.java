import java.util.Hashtable;
import java.util.Map;

public class AssignmentHashTable {
    public static void main(String[] args) {
        Hashtable<String,Double> student = new Hashtable<String, Double>();

        student.put("Tony", 75.8);
        student.put("Nara", 68.5);
        student.put("Sokha", 68.5);
        student.put("Narith", 73.3);
        student.put("Honda", 82.0);
        Object maxKey=null;
        Double maxValue = Double.MIN_VALUE;
        for(Map.Entry<String,Double> entry : student.entrySet()) {
            if(entry.getValue() > maxValue) {
                maxValue = entry.getValue();
                maxKey = entry.getKey();
            }
        }
        System.out.println("The highest mark in the class:\n"+maxKey+" = "+maxValue);

    }
}
