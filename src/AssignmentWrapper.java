public class AssignmentWrapper {
    public static void main(String[] args) {
        int i = 10;
        // create integer object
        Integer intObject = new Integer(i);
        System.out.println(intObject);
        // use class method to convert int to string
        System.out.println(intObject.toString());
    }
}
